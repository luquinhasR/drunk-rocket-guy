﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScript : MonoBehaviour
{
    private float screenWidth;
    void Start()
    {
        Rigidbody2D body = GetComponent<Rigidbody2D>();
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
//pegando o tamanho da imagem
        float imageWidth = renderer.sprite.bounds.size.x;

        float imageHeigth = renderer.sprite.bounds.size.y;

//pegando a altura da tela. orthographicSize pega metade da tela, x2 da a tela total.//;

//pegando a largura real da tela;
        float screenHeight = Camera.main.orthographicSize*2;
        this.screenWidth =screenHeight / Screen.height * Screen.width;
//Re escalando o tamanho da imagem para o tamnho da tela//;

        Vector2 newScale = this.transform.localScale;
        newScale.x = screenWidth / imageWidth + 0.25f;
        newScale.y = screenHeight / imageHeigth;
        this.transform.localScale = newScale;
    
        //adicionando movimento
        body.velocity = new Vector2(-3,0);

        if (this.name == "bluemoon_jump1")
        {
            this.transform.position = new Vector2(screenWidth, 0);
        }
    }



    void Update()
    {
        if (this.transform.position.x <= -screenWidth)
        {
            this.transform.position = new Vector2(screenWidth, 0);
        }
    }
}
